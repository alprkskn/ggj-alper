package  {
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.SpreadMethod;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.display.Sprite;
	import flash.net.FileReference;
	import com.adobe.images.PNGEncoder;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.ByteArray;
	import flash.text.TextFieldAutoSize;
	import com.adobe.serialization.json.JSON;
	/**
	$(CBI)* ...
	$(CBI)* @author alper keskin
	$(CBI)*/
	public class Editor extends Sprite {
		
		
		// Editor keyboard states...
		
		private var ctrlDown:Boolean = false;
		private var shiftDown:Boolean = false;
		private var spaceDown:Boolean = false;
		private var textureMode:Boolean = true;
		
		private var currentSelectTexture:int = Textures.WALL;
		private var currentSelectEntity:int = Items.PLAYER_SPAWNER;
		
		///////////////////
		
		// Screen properties
		
		private var zoomLevel:Number = 1.0;
		private var topLeftPos:Point;

		
		private const BOUND_COLOR:uint = 0xFFFFFFFF;
		private const CURRENT_COLOR:uint = 0xFF00FF00;
		private const BOUND_THICKNESS:int = 2;
		
		private var terrain:Sprite;
		private var mapBounds:Sprite;
		private var entitySprite:Sprite;
		private var currentChanges:Sprite;	
		private var gui:guiSprite;
		
		private var mapPolygons:Vector.<Polygon>; //Map boundary polygons' container.
		private var terrainPolygons:Vector.<Polygon>;
		private var entities:Vector.<Entity>;
		
		private var currentVertex:int = 0;
		private var currentPolygon:Vector.<Point>;
		
		private var wallBitmap:BitmapData;
		private var stoneBitmap:BitmapData;
		private var waterBitmap:BitmapData;
		private var dirtBitmap:BitmapData;
		
		private var hoverAnchor:Point = null;
		
		//GUI Objects

		
		/////////////
		
		public function Editor(g:guiSprite) {
			mapBounds = new Sprite();
			terrain = new Sprite();
			entitySprite = new Sprite();
			gui = g;
			currentChanges = new Sprite();
			mapPolygons = new Vector.<Polygon>();
			terrainPolygons = new Vector.<Polygon>();
			currentPolygon = new Vector.<Point>();
			entities = new Vector.<Entity>();
			addEventListener(Event.ENTER_FRAME, onFrame);
			
			addChild(terrain);
			addChild(mapBounds);
			addChild(entitySprite);
			addChild(currentChanges);
			
			wallBitmap = (new Textures.WALL_TEXTURE() as Bitmap).bitmapData;
			stoneBitmap = (new Textures.STONE_TEXTURE() as Bitmap).bitmapData;
			waterBitmap = (new Textures.WATER_TEXTURE() as Bitmap).bitmapData;
			dirtBitmap = (new Textures.DIRT_TEXTURE() as Bitmap).bitmapData;
			
			topLeftPos = new Point(0, 0);
			
			
			
		}
		
		public function onFrame(e:Event):void {
			currentChanges.graphics.clear();
			if(textureMode)
				gui.currentSelectionText.text = "Texture Mode - " + Textures.getName(currentSelectTexture);
			else
				gui.currentSelectionText.text = "Entity Mode - " + Items.getName(currentSelectEntity);
				
			if (currentPolygon.length != 0) {
				currentChanges.graphics.lineStyle(1, 0x000000, 1);
				if (currentPolygon.length == 1) {
					currentChanges.graphics.moveTo(currentPolygon[0].x, currentPolygon[0].y);
					currentChanges.graphics.lineTo(mouseX, mouseY);
				}
				
				if (currentPolygon.length == 2) {
					currentChanges.graphics.moveTo(currentPolygon[0].x, currentPolygon[0].y);
					currentChanges.graphics.lineTo(currentPolygon[1].x, currentPolygon[1].y);
					currentChanges.graphics.lineTo(mouseX, mouseY);
					currentChanges.graphics.lineTo(currentPolygon[0].x, currentPolygon[0].y);
				}
			}
		}
		public function onMouseMove(e:MouseEvent):void {
			var pos:Point = absolutePosition(e.stageX, e.stageY);
			if (hoverAnchor) {
				var delta:Point = new Point(e.stageX, e.stageY).subtract(hoverAnchor);
				this.x += delta.x * zoomLevel;
				this.y += delta.y * zoomLevel;
				
				topLeftPos.x = this.x;
				topLeftPos.y = this.y;
				
				hoverAnchor.x = e.stageX;
				hoverAnchor.y = e.stageY;
			}
		}
		
		public function onMouseDown(e:MouseEvent):void {
			var pos:Point = absolutePosition(e.stageX, e.stageY);
			if(textureMode) {	
				if (ctrlDown) {
					hoverAnchor = new Point(e.stageX, e.stageY);
				} else if (shiftDown) {
					
				} else if (spaceDown) {
					
				} else {
					if(currentSelectTexture == Textures.WALL) {
						if (currentVertex == 0) {
							currentPolygon.push(snapToNearestVertex(pos, mapPolygons));
							currentVertex++;
							
						} else if ( currentVertex == 1) {
							currentPolygon.push(snapToNearestVertex(pos, mapPolygons));
							currentVertex++;						
						} else if (currentVertex == 2) {
							currentPolygon.push(snapToNearestVertex(pos, mapPolygons));
							mapPolygons.push(new Polygon(currentPolygon[0], currentPolygon[1], currentPolygon[2]));				
							drawPolygon(mapBounds, mapPolygons[mapPolygons.length - 1], wallBitmap, true);
							
							currentPolygon = new Vector.<Point>();
							currentVertex = 0;
						}
					}
					if (currentSelectTexture == Textures.STONE) {
							if (currentVertex == 0) {
							currentPolygon.push(snapToNearestVertex(pos, terrainPolygons));
							currentVertex++;
							
						} else if ( currentVertex == 1) {
							currentPolygon.push(snapToNearestVertex(pos, terrainPolygons));
							currentVertex++;
							
						} else if (currentVertex == 2) {
							currentPolygon.push(snapToNearestVertex(pos, terrainPolygons));
							terrainPolygons.push(new Polygon(currentPolygon[0], currentPolygon[1], currentPolygon[2]));				
							drawPolygon(terrain, terrainPolygons[terrainPolygons.length - 1], stoneBitmap);
							
							currentPolygon = new Vector.<Point>();
							currentVertex = 0;
						}
					}
					if (currentSelectTexture == Textures.WATER) {
							if (currentVertex == 0) {
							currentPolygon.push(snapToNearestVertex(pos, terrainPolygons));
							currentVertex++;
							
						} else if ( currentVertex == 1) {
							currentPolygon.push(snapToNearestVertex(pos, terrainPolygons));
							currentVertex++;
							
						} else if (currentVertex == 2) {
							currentPolygon.push(snapToNearestVertex(pos, terrainPolygons));
							terrainPolygons.push(new Polygon(currentPolygon[0], currentPolygon[1], currentPolygon[2]));				
							drawPolygon(terrain, terrainPolygons[terrainPolygons.length - 1], waterBitmap);
							
							currentPolygon = new Vector.<Point>();
							currentVertex = 0;
						}
					}
					if (currentSelectTexture == Textures.DIRT) {
							if (currentVertex == 0) {
							currentPolygon.push(snapToNearestVertex(pos, terrainPolygons));
							currentVertex++;
							
						} else if ( currentVertex == 1) {
							currentPolygon.push(snapToNearestVertex(pos, terrainPolygons));
							currentVertex++;
							
						} else if (currentVertex == 2) {
							currentPolygon.push(snapToNearestVertex(pos, terrainPolygons));
							terrainPolygons.push(new Polygon(currentPolygon[0], currentPolygon[1], currentPolygon[2]));				
							drawPolygon(terrain, terrainPolygons[terrainPolygons.length - 1], dirtBitmap);
							
							currentPolygon = new Vector.<Point>();
							currentVertex = 0;
						}
					}
					
				}
			}
			else {
				if (ctrlDown) {
					hoverAnchor = new Point(e.stageX, e.stageY);
				} else if (shiftDown) {
					
				} else if (spaceDown) {
					
				} else {
					var en:Entity = new Entity(currentSelectEntity, new Point(pos.x, pos.y));
					if (currentSelectEntity == Items.PLAYER_SPAWNER) {
						if (entities.length != 0 && entities[0].type == Items.PLAYER_SPAWNER)
							entities.shift();
						entities.unshift(en);
					}
					else {
						entities.push(en);
					}
					
					if (currentSelectEntity == Items.PLAYER_SPAWNER) {
						var bmp:Bitmap = new Items.PL_SPAWNER_IMG;
						bmp.x = pos.x - bmp.bitmapData.width/2;
						bmp.y = pos.y - bmp.bitmapData.height / 2;
						entitySprite.addChild(bmp);
					}
					if (currentSelectEntity == Items.ZOMBIE_SPAWNER) {
						bmp = new Items.ZM_SPAWNER_IMG;
						bmp.x = pos.x - bmp.bitmapData.width/2;
						bmp.y = pos.y - bmp.bitmapData.height / 2;
						entitySprite.addChild(bmp);
					}
					if (currentSelectEntity == Items.LEVEL_FINISH) {
						bmp = new Items.LVL_FINISH_IMG;
						bmp.x = pos.x - bmp.bitmapData.width/2;
						bmp.y = pos.y - bmp.bitmapData.height / 2;
						entitySprite.addChild(bmp);
					}
				}
			}

		}
		
		public function onMouseUp(e:MouseEvent):void {
			if (ctrlDown) {
				hoverAnchor = null;
			}
		}
		
		public function onMouseWheel(e:MouseEvent):void {
			if (ctrlDown) {				
				if (e.delta < 0 && zoomLevel > 0.2) {
					this.scaleX -= 0.1;
					this.scaleY -= 0.1;
					zoomLevel -= 0.1;

				}
				
				if (e.delta > 0 && zoomLevel < 1.0){
					this.scaleX += 0.1;
					this.scaleY += 0.1;
					zoomLevel += 0.1;
					topLeftPos.x = this.x *= zoomLevel;
					topLeftPos.y = this.y *= zoomLevel;
				}
			}
			if (!ctrlDown) {
				if (textureMode) {
					if(e.delta < 0) {
						currentSelectTexture --;
					}
					else {
						currentSelectTexture++;
					}
					if (currentSelectTexture == -1)
						currentSelectTexture = 5;
					currentSelectTexture = currentSelectTexture % Textures.inventorySize;
				}
				else {
					if(e.delta < 0) {
						currentSelectEntity --;
					}
					else {
						currentSelectEntity++;
					}
					if (currentSelectEntity == -1)
						currentSelectEntity = 5;
					currentSelectEntity = currentSelectEntity % Items.inventorySize;
				}
			}
		}
		private function drawPolygon(sp:Sprite, p:Polygon, bmp:BitmapData, lined:Boolean = false):void {
			if(lined)
				sp.graphics.lineStyle(3, 0x000000, 1);
			else
				sp.graphics.lineStyle(0, 0x000000, 0);
			sp.graphics.beginBitmapFill(bmp);
			sp.graphics.moveTo(p.vertex_1.x, p.vertex_1.y);
			sp.graphics.lineTo(p.vertex_2.x, p.vertex_2.y);
			sp.graphics.lineTo(p.vertex_3.x, p.vertex_3.y);
			sp.graphics.lineTo(p.vertex_1.x, p.vertex_1.y);
			sp.graphics.endFill();
		}
		
		
		private function absolutePosition(x:Number, y:Number):Point {
			var p:Point = new Point(x, y);
			p = p.subtract(topLeftPos);
			p.x /= zoomLevel;
			p.y /= zoomLevel;
			return p;
		}
		
		private function snapToNearestVertex(p:Point, list:Vector.<Polygon>):Point {
			for each(var poly:Polygon in list) {
				if (isNearTo(p, poly.vertex_1, 20)) {
					return poly.vertex_1;
				} else if (isNearTo(p, poly.vertex_2, 20)) {
					return poly.vertex_2;
				} else if (isNearTo(p, poly.vertex_3, 20)) {
					return poly.vertex_3;
				}
			}
			return p;
		}
		
		private function isNearTo(object:Point, target:Point, dist:Number):Boolean {
			return (target.subtract(object).length < dist);
		}
		
		public function onKeyDown(e:KeyboardEvent):void {
			trace("keyCode", e.keyCode);
			if (currentPolygon.length == 0 && e.keyCode == 9) {
				textureMode = !textureMode;
			}
			
			if (e.keyCode == 17) {
				ctrlDown = true;
				trace("ctrl down");
			}
			if (e.keyCode == 32)
				spaceDown = true;
			if (e.keyCode == 16)
				shiftDown = true;
			
			
			if (!ctrlDown && e.keyCode == 83) {
				trace("save");
				saveMap("zaaa");
			}
			
			if (ctrlDown && e.keyCode == 83) {
				trace("savePng");
				var bmp:BitmapData = new BitmapData(2048, 2048, true);
				bmp.fillRect(bmp.rect, 0x00000000);
				this.removeChild(entitySprite);
				bmp.draw(this);
				this.addChild(entitySprite);
				savePng(bmp);				
			}
			if (e.keyCode == 82) {
				trace("reset");
				topLeftPos.x = this.x = 0;
				topLeftPos.y = this.y = 0;
				zoomLevel = 1.0;
				scaleX = scaleY = zoomLevel;
			}
		}
		
		public function onKeyUp(e:KeyboardEvent):void {
			if (e.keyCode == 17) {
				ctrlDown = false;
				trace("ctrl up");
			}
			if (e.keyCode == 32)
				spaceDown = false;
			if (e.keyCode == 16)
				shiftDown = false;
		}
		private function savePng(bmp:BitmapData):void {
			var file:FileReference = new FileReference();
			var byteArray: ByteArray = PNGEncoder.encode(bmp);
			var string:String = "Untitled.png";
			file.save(byteArray, string);
		}
		
		private function saveMap(name:String):void {
			var map:Map = new Map(name, mapPolygons, entities);
			var jsonText:String = JSON.encode(map);
			var file:FileReference = new FileReference();
			file.save(jsonText, name + "Data.txt");
			//file.name = name;
			//file.data = jsonText;
		}
	}

}

import flash.geom.Point;
import Box2D.Common.Math.b2Vec3;

class Polygon {
	public var vertex_1:Point;
	public var vertex_2:Point;
	public var vertex_3:Point;
	
	public function Polygon(a:Point, b:Point, c:Point) {
		vertex_1 = a;
		vertex_2 = b;
		vertex_3 = c;
		if (!isClockwise()) {
			var v2:Point = vertex_2.clone();
			var v3:Point = vertex_3.clone();
			vertex_3 = v2;
			vertex_2 = v3;
		}
	}
	
	private function isClockwise():Boolean {
		var e1:Point = vertex_1.subtract(vertex_2);
		var e2:Point = vertex_3.subtract(vertex_1);
		e1.normalize(1);
		e2.normalize(1);		
		
		var z:Number = e1.x * e2.y - e1.y * e2.x;
		return ( z < 0 );
	}
}

class Map {
	public var name:String;
	public var vertices:Array = new Array();
	public var entities:Array = new Array();
	
	public function Map(nm:String, wll:Vector.<Polygon>, ent:Vector.<Entity>) {
		name = nm;
		for each(var p:Polygon in wll) {
			vertices.push(p.vertex_1, p.vertex_2, p.vertex_3);
		}
		
		for each(var e:Entity in ent) {
			entities.push(e);
		}
	}
}

class Entity {
	
	public var type:int;
	public var name:String;
	public var pos:Point;
	
	public function Entity(t:int, p:Point) {
		name = Items.getName(type);
		type = t;
		pos = p;
	}
	
}