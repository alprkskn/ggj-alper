package {
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	
	/**
	$(CBI)* ...
	$(CBI)* @author alper keskin
	$(CBI)*/
	public class Main extends MovieClip {
		
		public function Main():void {
			var gui:guiSprite = new guiSprite();
			var editor:Editor = new Editor(gui);
			addChild(editor);
			addChild(gui);
			stage.addEventListener(MouseEvent.MOUSE_DOWN, editor.onMouseDown);
			stage.addEventListener(MouseEvent.MOUSE_UP, editor.onMouseUp);
			stage.addEventListener(MouseEvent.MOUSE_WHEEL, editor.onMouseWheel);
			stage.addEventListener(MouseEvent.MOUSE_MOVE, editor.onMouseMove);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, editor.onKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, editor.onKeyUp);
		}
	}
	
}