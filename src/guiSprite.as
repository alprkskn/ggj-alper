package  {
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	/**
	$(CBI)* ...
	$(CBI)* @author alper keskin
	$(CBI)*/
	public class guiSprite extends Sprite {
		public var GUITextFormat:TextFormat;
		public var currentSelectionText:TextField;
		public var currentZoomLevelText:TextField;
		
		public function guiSprite() {
			GUITextFormat = new TextFormat("Georgia", 10, 0x000000, true);
			currentSelectionText = new TextField();
			currentSelectionText.setTextFormat(GUITextFormat);
			currentSelectionText.x = currentSelectionText.y = 0;
			currentSelectionText.autoSize = TextFieldAutoSize.LEFT;
			
			addChild(currentSelectionText);
			
			addEventListener(Event.ENTER_FRAME, onFrame);
		}
		
		private function onFrame(e:Event):void {
			
		}
	}

}