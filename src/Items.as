package  {
	/**
	$(CBI)* ...
	$(CBI)* @author alper keskin
	$(CBI)*/
	public class Items {
		
		[Embed(source = "../assets/Misc/playerSpawner.png")]
		public static const  PL_SPAWNER_IMG:Class;
		
		[Embed(source = "../assets/Misc/zombieSpawner.png")]
		public static const ZM_SPAWNER_IMG:Class;
		
		[Embed(source = "../assets/Misc/levelFinish.png")]
		public static const  LVL_FINISH_IMG:Class;
		
		public static const PLAYER_SPAWNER:int = 0;
		public static const ZOMBIE_SPAWNER:int = 1;
		public static const LEVEL_FINISH:int = 2;
		
		public static var inventorySize:int = 3;
		
		public static function getName(i:int):String {
			switch(i) {
				case 0:	return "playerSpawner";
				case 1: return "zombieSpawner";
				case 2: return "levelFinish";
				default: return "Null";
			}
		}		
	}

}